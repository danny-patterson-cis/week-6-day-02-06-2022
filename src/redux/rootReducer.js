import { combineReducers } from 'redux'
import GetUserReducer from './reducer/GetUserReducer';
import UserEditReducer from './reducer/UserEditReducer';

const rootReducer = combineReducers({
    loadUsers: GetUserReducer,
    editUser: UserEditReducer,
});

export default rootReducer