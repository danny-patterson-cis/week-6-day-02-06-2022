import * as types from "../types/Types"
const initialState = {
    users: [],
    user: {},
    loading: true,

}

const UserAddReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_USER:
            return {
                ...state,
                loading: false,
            }
        default: return state;

    }

}

export default UserAddReducer;