import * as types from "../types/Types"
const initialState = {
    users: [],
    user: {},
    loading: true,

}

const GetUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_USERS:
            return {
                ...state,
                users: action.data,
                loading: false
            }
        default: return state;
    }
}

export default GetUserReducer;