import * as types from "../types/Types"
const initialState = {
    users: [],
    user: {},
    loading: true,

}

const UserEditReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.EDIT_USER:
            return {
                ...state,
                user: action.data,
                loading: false
            }
        default: return state;

    }

}

export default UserEditReducer;