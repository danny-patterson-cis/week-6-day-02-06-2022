import * as types from "../types/Types"
const initialState = {
    users: [],
    user: {},
    loading: true,

}

const UpdateUserData = (state = initialState, action) => {
    switch (action.type) {
        case types.UPDATE_USERS_DATA:
            return {
                ...state,
                loading: false,
            }
        default: return state;

    }

}

export default UpdateUserData;