import * as types from "../types/Types"
const initialState = {
    users: [],
    user: {},
    loading: true,

}

const UserDeletedReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.DELETE_USER:
            return {
                ...state,
                loading: false,
            }
        default: return state;

    }

}

export default UserDeletedReducer;