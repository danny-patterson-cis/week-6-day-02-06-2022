import * as types from "../types/Types"
import axios from "axios";
import { loadUsers } from "./GetUsersAction";

const userUpdated = () => ({
    type: types.UPDATE_USERS_DATA,
});

export const updateUser = (user, id) => {
    return function (dispatch) {
        axios.put(`${process.env.REACT_APP_API}/${id}`, user).then((response) => {
            console.log("edit", response)
            dispatch(userUpdated());
            dispatch(loadUsers());
        }).catch(error => console.log(error))
    }
}