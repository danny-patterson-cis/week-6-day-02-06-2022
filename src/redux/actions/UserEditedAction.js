import * as types from "../types/Types"
import axios from "axios";

const userEdited = (user) => ({
    type: types.EDIT_USER,
    data: user,
});

export const editUser = (id) => {
    return function (dispatch) {
        axios.get(`${process.env.REACT_APP_API}/${id}`).then((response) => {
            console.log("edit", response)
            dispatch(userEdited(response.data));
        }).catch(error => console.log(error))
    }
}