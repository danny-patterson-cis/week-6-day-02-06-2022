import * as types from "../types/Types"
import axios from "axios";
import { loadUsers } from "./GetUsersAction";

const userAdded = () => ({
    type: types.ADD_USER,
});

export const addUser = (user) => {
    return function (dispatch) {
        axios.post(`${process.env.REACT_APP_API}`, user).then((response) => {
            console.log("response", response)
            dispatch(userAdded());
            dispatch(loadUsers());
        }).catch(error => console.log(error))
    }
}