import * as types from "../types/Types"
import axios from "axios";
import { loadUsers } from './GetUsersAction'

const userDelete = () => ({
    type: types.DELETE_USER,
});

export const deleteUser = (id) => {
    return function (dispatch) {
        axios.delete(`${process.env.REACT_APP_API}/${id}`).then((response) => {
            console.log("response", response)
            dispatch(userDelete());
            dispatch(loadUsers());
        }).catch(error => console.log(error))
    }
}