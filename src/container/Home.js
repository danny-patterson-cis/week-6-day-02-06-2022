import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { loadUsers } from '../redux/actions/GetUsersAction'
import { deleteUser } from '../redux/actions/UserDeleteAction'
import { addUser } from '../redux/actions/UserAddedAction'
import { editUser } from '../redux/actions/UserEditedAction'
import { updateUser } from '../redux/actions/UpdatedUserAction'
import '../container/Home.css'

export const Home = () => {
    const emptyInput = {
        fname: "",
        lname: "",
        email: "",
        gender: "",
        specialization: ""
    };
    const [inputValue, setInputValue] = useState(emptyInput);
    const [error, setError] = useState("");
    const { users } = useSelector(state => state.loadUsers);
    const { user } = useSelector(state => state.editUser);
    const { fname, lname, email, gender, specialization } = inputValue;
    const dispatch = useDispatch();

    const onChangeHandler = (e) => {
        setInputValue({ ...inputValue, [e.target.name]: e.target.value })
    }

    useEffect(() => {
        dispatch(loadUsers());
    }, []);

    useEffect(() => {
        if (user) {
            setInputValue(user)
        }
    }, [user])

    const onSubmit = (e) => {
        e.preventDefault();
        if (!fname || !lname || !email || !gender || !specialization) {
            setError("Please fill the All the Data");
        } else {
            if (!user.id) {
                dispatch(addUser(inputValue));
                setError("");
                setInputValue(emptyInput);
            }
            else {
                dispatch(updateUser(inputValue, user.id));
                setInputValue(emptyInput);
            }
        }
    }

    const deleteFunction = (id) => {
        if (window.confirm("Are you sure you want to delete the data?")) {
            dispatch(deleteUser(id))
        }
    }

    const editFunction = (id) => {
        dispatch(editUser(id));
    }

    return (
        <div className='bg-info bg-gradient fw-bold'>
            <div className='container' >
                <form>
                    <div className="mb-3" >
                        <label htmlFor="exampleInputEmail1" className="form-label">First Name</label>
                        <input type="text" className="form-control" name="fname" id="fname" value={fname || ""} onChange={(e) => onChangeHandler(e)} placeholder="Your First Name" />
                        {error && <span className="text-danger">{error}</span>}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleInputPassword1" className="form-label">Last Name</label>
                        <input type="text" className="form-control" name="lname" id="lname" value={lname || ""} onChange={(e) => onChangeHandler(e)} placeholder="Your Last Name" />
                        {error && <span className="text-danger">{error}</span>}
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput1" className="form-label" >Email address</label>
                        <input type="email" className="form-control" name="email" id="email" value={email || ""} onChange={(e) => onChangeHandler(e)} placeholder="name@example.com" />
                        {error && <span className="text-danger">{error}</span>}
                    </div>
                    <fieldset className="row mb-3">
                        <div className="col-sm-10">
                            <div className="col-form-label  ">
                                <div className='ms-5'>
                                    Please Select Your Gender
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" label="Male" name="gender" id="Male" value="Male" checked={gender === "Male" || ""} onChange={(e) => onChangeHandler(e)} />
                                    <label className="form-check-label" htmlFor="gridRadios1">
                                        Male
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" label="Female" name="gender" id="Female" value="Female" checked={gender === "Female" || ""} onChange={(e) => onChangeHandler(e)} />
                                    <label className="form-check-label" htmlFor="gridRadios2">
                                        Female
                                    </label>
                                </div>
                                {error && <span className="text-danger">{error}</span>}
                            </div>
                        </div>
                    </fieldset>
                    <select className="form-select" aria-label="Default select example" name="specialization" value={specialization || ""} onChange={(e) => onChangeHandler(e)} >
                        <option >Please Select Your Specialization</option>
                        <option value="Front-end Developer">Front-end Developer </option>
                        <option value="Back-end Developer">Back-end Developer</option>
                        <option value="Full-stack Developer">Full-stack Developer</option>
                    </select>
                    {error && <span className="text-danger">{error}</span>}
                    <div>
                        <button className="btn btn-primary mt-3" onClick={onSubmit}>Submit</button>
                    </div>
                </form>
            </div>
            <table className="table table-dark table-striped mt-5">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Specialization</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody >
                    {users && users.map((d) => (
                        <tr className="table table-striped table-info" key={d.id}>
                            <th scope="row">{d.id}</th>
                            <td>{d.fname}</td>
                            <td>{d.lname}</td>
                            <td>{d.email}</td>
                            <td>{d.gender}</td>
                            <td>{d.specialization}</td>
                            <td><button type="button" className="btn btn-primary" onClick={() => editFunction(d.id)}>Edit</button></td>
                            <td><button type="button" className="btn btn-danger " onClick={() => deleteFunction(d.id)} >Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}
